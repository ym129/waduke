package model;

import java.sql.Timestamp;

public class Membership {
	private int membershipNumber;
	private String accountStatus;
	private String  classification;	
	private String membershipType;
	private Timestamp dateIssued;
	private Timestamp memberSince;
	private int yearsMember;
	private Timestamp expires;
	private String specificReasonMemberDropped1;
	private String specificReasonMemberDropped2;
	private String overallReasonMemberDropped;	
	public int getMembershipNumber(){
		return membershipNumber;
	}
	public void setMembershipNumber(int membershipNumber){
		this.membershipNumber = membershipNumber;
	}
	public String getAccountStatus(){
		return accountStatus;
	}
	public void setAccountStatus(String accountStatus){
		this.accountStatus = accountStatus;
	}
	public String getClassification(){
		return classification;
	}
	public void setClassification(String classification){
		this.classification = classification;
	}
	public String getMembershipType(){
		return membershipType;
	}
	public void setMembershipType(String membershipType){
		this.membershipType = membershipType;
	}
	public Timestamp getDateIssued(){
		return dateIssued;
	}
	public void setDateIssued(Timestamp dateIssued){
		this.dateIssued = dateIssued;
	}
	public Timestamp getMemberSince(){
		return memberSince;
	}
	public void setMemberSince(Timestamp memberSince){
		this.memberSince = memberSince;
	}
	public int getYearsMember(){
		return yearsMember;
	}
	public void setYearsMember(int yearsMember){
		this.yearsMember = yearsMember;
	}
	public Timestamp getExpires(){
		return expires;
	}
	public void setExpires(Timestamp expires){
		this.expires = expires;
	}
	public String getSpecificReasonMemberDropped1(){
		return specificReasonMemberDropped1;
	}
	public void setSpecificReasonMemberDropped1(String specificReasonMemberDropped1){
		this.specificReasonMemberDropped1 = specificReasonMemberDropped1;
	}	
	public String getSpecificReasonMemberDropped2(){
		return specificReasonMemberDropped2;
	}
	public void setSpecificReasonMemberDropped2(String specificReasonMemberDropped2){
		this.specificReasonMemberDropped2 = specificReasonMemberDropped2;
	}
	public String getOverallReasonMemberDropped(){
		return overallReasonMemberDropped;
	}
	public void setOverallReasonMemberDropped(String overallReasonMemberDropped){
		this.overallReasonMemberDropped = overallReasonMemberDropped;
	}
	public Object[] getAll(){
		Object[] ans = {accountStatus,classification,membershipType,dateIssued,memberSince,
			yearsMember,expires,specificReasonMemberDropped1,specificReasonMemberDropped2,
			overallReasonMemberDropped};
		return ans;
	}
}