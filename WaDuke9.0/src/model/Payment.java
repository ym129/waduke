package model;

import java.sql.Timestamp;

public class Payment {
	private int paymentID;
	private String numberOfCard;
	private int paymentAmount;
	private String paymentTyep;
	private String checkAcctOnline;
	private String receivedVia;	
	private Timestamp datePaymentMake;
	private Timestamp datePacketSent;
	private Timestamp secondDateCardSent;

	public int getPaymentID(){
		return paymentID;
	}
	public void setPaymentID(int ID){
		paymentID = ID;
	}
	public String getNumberOfCard(){
		return numberOfCard;
	}
	public void setNumberOfCard(String numberOfCard){
		this.numberOfCard = numberOfCard;
	}
	public int getPaymentAmount(){
		return paymentAmount;
	}
	public void setPaymentAmount(int paymentAmount){
		this.paymentAmount = paymentAmount;
	}	
	public String getPaymentType(){
		return paymentTyep;
	}
	public void setPaymentType(String paymentTyep){
		this.paymentTyep = paymentTyep;
	}
	public String getCheckAcctOnline(){
		return checkAcctOnline;
	}
	public void setCheckAcctOnline(String checkAcctOnline){
		this.checkAcctOnline = checkAcctOnline;
	}
	public String getReceivedVia(){
		return receivedVia;
	}
	public void setReceivedVia(String receivedVia){
		this.receivedVia = receivedVia;
	}
	public Timestamp getDatePaymentMake(){
		return datePaymentMake;
	}
	public void setDatePaymentMake(Timestamp datePaymentMake){
		this.datePaymentMake = datePaymentMake;
	}
	public Timestamp getDatePaymentSent(){
		return datePaymentSent;
	}
	public void setDatePaymentSent(Timestamp datePaymentSent){
		this.datePaymentSent = datePaymentSent;
	}
	public Timestamp getSecondDateCardSent(){
		return secondDateCardSent;
	}
	public void setSecondDateCardSent(Timestamp secondDateCardSent){
		this.secondDateCardSent = secondDateCardSent;
	}
	public Object[] getAll(){
		Object[] ans = {numberOfCard,paymentAmount,paymentTyep,checkAcctOnline,
			receivedVia,datePaymentMake,datePacketSent,secondDateCardSent};
		return ans;
	}
}