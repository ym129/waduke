package model;

import java.sql.Timestamp;

public class Transaction {
	private int transactionID;
	private String table;
	private String primaryKey;
	private String field;
	private String action;
	private String originalInfo;
	private String newInfo;

	public int getTransactionID(){
		return transactionID;
	}
	public void setTransactionID(int ID){
		transactionID = ID;
	}
	public String getTable(){
		return table;
	}
	public void setTable(String table){
		this.table = table;
	}
	public String getPrimarykey(){
		return primarykey;
	}
	public void setPrimarykey(String primarykey){
		this.primarykey = primarykey;
	}
	public String getField(){
		return field;
	}
	public void setField(String field){
		this.field = field;
	}
	public String getAction(){
		return action;
	}
	public void setAction(String action){
		this.action = action;
	}
	public String getOriginalInfo(){
		return originalInfo;
	}
	public void setOriginalInfo(String originalInfo){
		this.originalInfo = originalInfo;
	}
	public String getNewInfo(){
		return newInfo;
	}
	public void setNewInfo(String newInfo){
		this.newInfo = newInfo;
	}
	public Object[] getAll(){
		Object[] ans = {table,primaryKey,field,action,originalInfo,newInfo};
		return ans;
	}
}