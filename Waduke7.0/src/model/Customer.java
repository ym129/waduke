package model;

import java.sql.Timestamp;

public class Customer {
	private int custormerID;
	private String salutation;
	private String firstName;
	private String middleName;
	private String nickName;
	private String lastName;
	private String suffix;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zip;
	private String country;
	private String workPhone;
	private String homePhone;
	private String cellPhone;
	private String intlPhone;
	private String primaryEmail;
	private String alternativeEmail;
	private String company;
	private String positionHeld;
	private Timestamp birthday;
	private String referredBy1;	
	private String referredBy2;
	private String purposeForVisiting1;
	private String purposeForVisiting2;
	private boolean dukeParent;
	private String gradYear;
	private boolean dukeStudentAthlete;
	private String sport;
	private boolean dukeAlumn;
	private boolean playGolf;

	public int getCustomerID(){
		return custormerID;
	}
	public void setCustomerID(int ID){
		custormerID = ID;
	}

	public String getSalutation(){
		return salutation;
	}
	public void setSalutation(String salutation){
		this.salutation = salutation;
	}
	public String getFirstName(){
		return firstName;
	}
	public void setFirstName(String firstName){
		this.firstName = firstName;
	}
	public String getMiddleName(){
		return middleName;
	}
	public void setMiddleName(String middleName){
		this.middleName = middleName;
	}
	public String getNickName(){
		return nickName;
	}
	public void setNickName(String nickName){
		this.nickName = nickName;
	}
	public String getLastName(){
		return lastName;
	}
	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getSuffix(){
		return suffix;
	}
	public void setSuffix(String suffix){
		this.suffix = suffix;
	}
	public String getAddress1(){
		return address1;
	}
	public void setAddress1(){
		this.address1 = address1;
	}
	public String getAddress2(){
		return address2;
	}
	public void setAddress2(){
		this.address2 = address2;
	}
	public String getCity(){
		return city;
	}
	public void setCity(String city){
		this.city = city;
	}
	public String getState(){
		return state;
	}
	public void setState(String state){
		this.state = state;
	}
	public String getZip(){
		return zip;
	}
	public void setZip(String zip){
		this.zip = zip;
	}
	public String getCountry(){
		return country;
	}
	public void setCountry(String country){
		this.country = country;
	}
	public String getWorkPhone(){
		return workPhone;
	}
	public void setWorkPhone(String workPhone){
		this.workPhone = workPhone;
	}
	public String getHomePhone(){
		return homePhone;
	}
	public void setHomePhone(String homePhone){
		this.homePhone = homePhone;
	}
	public String getCellPhone(){
		return cellPhone;
	}
	public void setCellPhone(String cellPhone){
		this.cellPhone = cellPhone;
	}

	public String getIntlPhone(){
		return intlPhone;
	}
	public void setIntlPhone(String intlPhone){
		this.intlPhone = intlPhone;
	}
	public String getPrimaryEmail(){
		return primaryEmail;
	}
	public void setPrimaryEmail(String primaryEmail){
		this.primaryEmail = primaryEmail;
	}
	public String getAlternativeEmail(){
		return alternativeEmail;
	}
	public void setAlternativeEmail(String alternativeEmail){
		this.alternativeEmail = alternativeEmail;
	}
	public String getCompany(){
		return company;
	}
	public void setCompany(String company){
		this.company = company;
	}
	public String getPositionHeld(){
		return positionHeld;
	}
	public void setPositionHeld(String positionHeld){
		this.positionHeld = positionHeld;
	}
	public Timestamp getBirthday(){
		return birthday;
	}
	public void setBirthday(Timestamp birthday){
		this.birthday = birthday;
	}
	public String getReferredBy1(){
		return referredBy1;
	}
	public void setReferredBy1(String referredBy1){
		this.referredBy1 = referredBy1;
	}
	public String getReferredBy2(){
		return referredBy2;
	}
	public void setReferredBy2(String referredBy2){
		this.referredBy2 = referredBy2;
	}
	public String getPurposeForVisiting1(){
		return purposeForVisiting1;
	}
	public void setPurposeForVisiting1(String purposeForVisiting1){
		this.purposeForVisiting1 = purposeForVisiting1;
	}
	public String getPurposeForVisiting2(){
		return purposeForVisiting2;
	}
	public void setPurposeForVisiting2(String purposeForVisiting2){
		this.purposeForVisiting2 = purposeForVisiting2;
	}
	public boolean getDukeParent(){
		return dukeParent;
	}
	public void setDukeParent(boolean dukeParent){
		this.dukeParent = dukeParent;
	}
	public String getGradYear(){
		return gradYear;
	}
	public void setGradYear(String gradYear){
		this.gradYear = gradYear;
	}
	public boolean getDukeStudentAthlete(){
		return dukeStudentAthlete;
	}
	public void setDukeStudentAthlete(boolean dukeStudentAthlete){
		this.dukeStudentAthlete = dukeStudentAthlete;
	}
	public String getSport(){
		return sport;
	}
	public void setSport(String sport){
		this.sport = sport;
	}
	public boolean getDukeAlumn(){
		return dukeAlumn;
	}
	public void setDukeAlumn(boolean dukeAlumn){
		this.dukeAlumn = dukeAlumn;
	}
	public boolean getPlayGolf(){
		return playGolf;
	}
	public void setPlayGolf(boolean playGolf){
		this.playGolf = playGolf;
	}
	public Object[] getAll(){
		Object[] ans = {salutation,firstName,middleName,nickName,lastName,suffix,
			address1,address2,city,state,zip,country,
			workPhone,homePhone,cellPhone,intlPhone,primaryEmail,alternativeEmail,
			company,positionHeld,birthday,referredBy1,referredBy2,
			purposeForVisiting1,purposeForVisiting2,
			dukeParent,gradYear,dukeStudentAthlete,sport,dukeAlumn,playGolf};
		return ans;
	}
}


