package model;

import java.sql.Timestamp;

public class Dining {
	private int diningID;
	private Timestamp favoriteDiningTime;
	private String tableLocation;
	private String wine;
	private String varietal;
	private String favoriteServer;
	private String dietaryNeeds;
	private String foodAllegies;
	private String eventsInterestedIn1;
	private String eventsInterestedIn2;
	private String eventsAttended;

	public int getDiningID(){
		return diningID;
	}
	public void setDiningID(int ID){
		diningID = ID;
	}
	public Timestamp getFavoriteDiningTime(){
		return favoriteDiningTime;
	}
	public void setFavoriteDiningTime(Timestamp favoriteDiningTime){
		this.favoriteDiningTime = favoriteDiningTime;
	}
	public String getTableLocation(){
		return tableLocation;
	}
	public void setTableLocation(String tableLocation){
		this.tableLocation = tableLocation;
	}
	public String getWine(){
		return wine;
	}
	public void setWine(String wine){
		this.wine = wine;
	}
	public String getVarietal(){
		return varietal;
	}
	public void setVarietal(String varietal){
		this.varietal = varietal;
	}
	public String getFavoriteServer(){
		return favoriteServer;
	}
	public void setFavoriteServer(String favoriteServer){
		this.favoriteServer = favoriteServer;
	}
	public String getDietaryNeeds(){
		return dietaryNeeds;
	}
	public void setDietaryNeeds(String dietaryNeeds){
		this.dietaryNeeds = dietaryNeeds;
	}
	public String getFoodAllegies(){
		return foodAllegies;
	}
	public void setFoodAllegies(String foodAllegies){
		this.foodAllegies = foodAllegies;
	}
	public String getEventsInterestedIn1(){
		return eventsInterestedIn1;
	}
	public void setEventsInterestedIn1(String eventsInterestedIn1){
		this.eventsInterestedIn1 = eventsInterestedIn1;
	}
	public String getEventsInterestedIn2(){
		return eventsInterestedIn2;
	}
	public void setEventsInterestedIn2(String eventsInterestedIn2){
		this.eventsInterestedIn2 = eventsInterestedIn2;
	}
	public String getEventsAttended(){
		return eventsAttended;
	}
	public void setEventsAttended(String eventsAttended){
		this.eventsAttended = eventsAttended;
	}
	public Object[] getAll(){
		Object[] ans = {favoriteDiningTime,tableLocation,wine,varietal,favoriteServer,
			dietaryNeeds,foodAllegies,eventsInterestedIn1,eventsInterestedIn2,eventsAttended};
		return ans;
	}
}