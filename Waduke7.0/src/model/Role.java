package model;

import java.sql.Timestamp;

public class Role {
	private int roleID;
	private int customerID;
	private int membershipID;
	private boolean isOwner;
	public int getRoleID(){
		return roleID;
	}
	public void setRoleID(int ID){
		roleID = ID;
	}
	public int getCustomerID(){
		return customerID;
	}
	public void setCustomerID(int ID){
		customerID = ID;
	}
	public int getMembershipID(){
		return membershipID;
	}
	public void setMembershipID(int ID){
		membershipID = ID;
	}
	public boolean getIsOwner(){
		return isOwner;
	}
	public void setIsOwner(boolean isOwner){
		this.isOwner = isOwner;
	}
	public Object[] getAll(){
		Object[] ans = {customerID,membershipID,isOwner};
		return ans;
	}
}