package com.dede.app1st.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import model.testuser;

/**
 * Servlet implementation class SignUp
 */
@WebServlet(description = "sign up page", urlPatterns = { "/SignUp.do" })
public class SignUp extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SignUp() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Configuration config = new Configuration().configure();
		
		ServiceRegistry servReg = new StandardServiceRegistryBuilder().applySettings(config.getProperties()).build();
		SessionFactory factory = config.buildSessionFactory(servReg);
		Session session = factory.openSession();
		session.beginTransaction();
		testuser u = new testuser(request.getParameter("firstname"), request.getParameter("lastname"), request.getParameter("country"));
		
		session.save(u);
		session.getTransaction().commit();
		String hql =("select user from testuser as user where user.firstname like :thename");
		String firstname=request.getParameter("firstname");
	
		
		List list=session.createQuery(hql).setParameter("thename", firstname).list();
		session.close();
		testuser newuser=(testuser) list.get(0);
		 request.setAttribute("newuser", newuser); 
		PrintWriter  out= response.getWriter();
        
        out.println("<body><h1>");
		out.println("Welcome ");
		out.println(firstname);
		out.println("Your id is");
		out.println(newuser.getId());
		out.println("</h1></body>");
		//RequestDispatcher view = request.getRequestDispatcher("signup.jsp");
		//view.forward(request, response);
	}

}
