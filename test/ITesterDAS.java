package com.WaDuke.test;

import java.sql.SQLException;
import java.util.ArrayList;

import com.WaDuke.test.Tester;

public interface ITesterDAO
{
    public int addTester(Tester tester, Connection conn) throws SQLException;
    
    public int removeTester(String string, Connection conn) throws SQLException;
    
    public int modifyTester(Tester tester, Connection conn) throws SQLException;

    public Tester findTesterByID(int testID, Connection conn) throws SQLException;

    public Tester findTesterByEmail(String primaryEmail, Connection conn) throws SQLException;

	public boolean TesterIDIsExisting(int testerID, Connection conn) throws SQLException;

	public boolean TesterEmailIsExisting(String primaryEmail, Connection conn) throws SQLException;
    //not finish
    public ArrayList findTesters(Object[] parameters, Connection conn) throws SQLException;
}