CREATE TABLE Test2(
	testerID serial not null,
	primaryEmail varchar not null,
	age int not null,
	city varchar,
	isRight boolean not null,
	UNIQUE(primaryEmail),
	PRIMARY KEY(testerID)
);