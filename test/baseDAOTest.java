package test.DAO;

import bean.Tester;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.AfterClass;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import DAO.baseDAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;

/**
 * baseDAO Tester.
 *
 * @author <Authors name>
 * @version 1.0
 * @since <pre>Mar 31, 2019</pre>
 */
public class baseDAOTest {
    static String url = "jdbc:postgresql://localhost:5433/testdb651";
    static String usr = "anqiwang";
    static String psd = "abc123";
    baseDAO testDAO = new baseDAO();
    static Connection conn = null;

    @BeforeClass
    public static void beforeClass() {
        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(url, usr, psd);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void afterClass() {
        try {
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Before
    public void before() throws Exception {
    }

    @After
    public void after() throws Exception {
    }

    /**
     * Method: basicOperation(String sql, Object[] parameters, Connection conn)
     */
    @Test
    public void testBasicOperationInsert() throws Exception {
//TODO: Test goes here...
        String sql = "INSERT INTO Test2 VALUES(DEFAULT, ?, ?, ?, ?)";
        Object[] parameters = {"test1", 10, "durham", false};
        testDAO.basicOperation(sql, parameters, conn);
        sql = "SELECT * FROM Test2 WHERE NAME = ?";
        Object[] parameters2 = {"test1"};
        Tester a = (Tester) testDAO.findObj(sql, parameters2, Tester.class, conn);
        assertEquals("test1", a.getName());
        assertEquals(10, a.getAge());
        assertEquals("durham", a.getCity());
        assertEquals(false, a.getIsright());
    }
    @Test
    public void testBasicOperationInsertNull() throws Exception {
//TODO: Test goes here...
        String sql = "INSERT INTO Test2 VALUES(DEFAULT, ?, ?, ?, ?)";
        Object[] parameters = {"test2", 10, null, false};
        testDAO.basicOperation(sql, parameters, conn);
        sql = "SELECT * FROM Test2 WHERE NAME = ?";
        Object[] parameters2 = {"test2"};
        Tester a = (Tester) testDAO.findObj(sql, parameters2, Tester.class, conn);
        assertEquals("test2", a.getName());
        assertEquals(10, a.getAge());
        assertNull(a.getCity());
        assertEquals(false, a.getIsright());
    }
    @Test
    public void testBasicOperationUpdate() throws Exception {
//TODO: Test goes here...
        String sql = "Update Test2 set AGE = ? WHERE NAME = ?";
        Object[] parameters = {20, "test1"};
        testDAO.basicOperation(sql, parameters, conn);
        sql = "SELECT * FROM Test2 WHERE NAME = ?";
        Object[] parameters2 = {"test1"};
        Tester a = (Tester) testDAO.findObj(sql, parameters2, Tester.class, conn);
        assertEquals("test1", a.getName());
        assertEquals(20, a.getAge());
        assertEquals("durham", a.getCity());
        assertEquals(false, a.getIsright());
    }
    @Test
    public void testBasicOperationUpdateMult() throws Exception {
//TODO: Test goes here...
        String sql = "Update Test2 set AGE = ?, name = ? WHERE NAME = ?";
        Object[] parameters = {20, "test1", "test2"};
        testDAO.basicOperation(sql, parameters, conn);
        sql = "SELECT * FROM Test2 WHERE NAME = ?";
        Object[] parameters2 = {"test1"};
        Tester a = (Tester) testDAO.findObj(sql, parameters2, Tester.class, conn);
        assertEquals("test1", a.getName());
        assertEquals(20, a.getAge());
        assertEquals("boston", a.getCity());
        assertEquals(false, a.getIsright());
    }
    @Test
    public void testBasicOperationDelete() throws Exception {
//TODO: Test goes here...
        String sql = "DELETE FROM TEST2 WHERE NAME = ?";
        Object[] parameters = {"test1"};
        testDAO.basicOperation(sql, parameters, conn);
        sql = "SELECT * FROM Test2 WHERE NAME = ?";
        Object[] parameters2 = {"test1"};
        Tester a = (Tester) testDAO.findObj(sql, parameters2, Tester.class, conn);
        assertNull(a);
    }
    @Test
    public void testBasicOperation() throws Exception {
//TODO: Test goes here...
        //see above
    }

    /**
     * Method: update(String sql, Object[] parameters, Class clazz, Connection conn)
     */
    @Test
    public void testUpdate() throws Exception {
//TODO: Test goes here...
        //delete
    }

    /**
     * Method: findObj(String sql, Object[] params, Class clazz, Connection conn)
     */
    @Test
    public void testFindObj() throws Exception {
//TODO: Test goes here... 
    }

    /**
     * Method: findObjs(String sql, Object[] params, Class clazz, Connection conn)
     */
    @Test
    public void testFindObjsMultRes() throws Exception {
//TODO: Test goes here...
        String sql = "SELECT * FROM TEST2 WHERE AGE = ?";
        Object[] parameters = {20};
        ArrayList<Tester> a = testDAO.findObjs(sql, parameters, Tester.class, conn);
        assertEquals(2, a.size());
    }
    @Test
    public void testFindObjsMultCri() throws Exception {
//TODO: Test goes here...
        String sql = "SELECT * FROM TEST2 WHERE AGE = ? AND ISRIGHT = ?";
        Object[] parameters = {20, false};
        ArrayList<Tester> a = testDAO.findObjs(sql, parameters, Tester.class, conn);
        assertEquals(1, a.size());
        assertEquals(20, a.get(0).getAge());
        assertEquals(false, a.get(0).getIsright());
    }
    @Test
    public void testFindObjs() throws Exception {
//TODO: Test goes here... 
    }

    /**
     * Method: isExisting(String sql, Object[] parameters, Connection conn)
     */
    @Test
    public void testIsExisting() throws Exception {
//TODO: Test goes here... 
    }


    /**
     * Method: mappingObj(ResultSet rs, Class clazz)
     */
    @Test
    public void testMappingObj() throws Exception {
//TODO: Test goes here... 
/* 
try { 
   Method method = baseDAO.getClass().getMethod("mappingObj", ResultSet.class, Class.class); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/
    }

} 
