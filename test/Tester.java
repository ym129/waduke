public class Tester {
    private int testerID;//primary key
    private String primaryEmail;//no duplicate
    private int age;
    private String city;
    private boolean isRight;
    public String getTesterID(){
        return testerID;
    }
    public void setTesterID(String testerID){
        this.testerID = testerID;
    }
    public String getPrimaryEmail(){
        return primaryEmail;
    }
    public void setPrimaryEmail(String primaryEmail){
        this.primaryEmail = primaryEmail;
    }
    public int getAge(){
        return age;
    }
    public void setAge(int age){
        this.age = age;
    }
    public String getCity(){
        return city;
    }
    public void setCity(String city){
        this.city = city;
    }
    public boolean getIsRight(){
        return isRight;
    }
    public void setIsRight(boolean isRight){
        this.isRight = isRight;
    }
    public Object[] getAll(){
        Object[] ans = {primaryEmail, age, city, isRight};
        return ans;
    }
}
