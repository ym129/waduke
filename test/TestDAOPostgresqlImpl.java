package com.WaDuke.test;

import java.sql.SQLException;
import java.util.ArrayList;
import com.WaDuke.test.Tester;

public class TesterDAOPostgresqlImpl extends baseDAO implements ITesterDAO {

	@Override
	public int addTester(Tester tester, Connection conn) throws SQLException {
		String sql = "INSERT INTO Tester VALUES(" + 
					 "?, ?, ?, ?)";
		Object[] parameters = tester.getAll();
		return basicOperation(sql, parameters, conn);
	}

	@Override
	public int removeTester(String primaryEmail, Connection conn) throws SQLException {
		String sql = "DELETE FROM Tester WHERE primaryEmail = ?";
		Object[] parameters = {primaryEmail};
		return basicOperation(sql, parameters, conn);
	}
	@Override
	public int updateTester(Tester tester, Connection conn) throws SQLException {
		String sql = "UPDATE Tester SET primaryEmail =?, age=?, city=?, "+
		"isRight=? where TesterID = ?";
		Object[] parameters = Tester.getAll();
		parameters = Array.copyOf(parameters, parameters.length+1);
		parameters[parameters.length-1] = tester.getTesterID();
		return basicOperation(sql, parameters, conn);
	}

	@Override
	public Tester findTesterByID(int TesterID, Connection conn) throws SQLException {
		String sql = "SELECT * FROM Tester WHERE TesterID = ?";
		Object[] parameters = {TesterID};
		return (Tester) findObj(sql, parameters, Tester.class, conn);
	}

	@Override
	public Tester findTesterByEmail(String primaryEmail, Connection conn) throws SQLException {
		String sql = "SELECT * FROM Tester WHERE primaryEmail = ?";
		Object[] parameters = {primaryEmail};
		return (Tester) findObj(sql, parameters, Tester.class, conn);
	}
	@Override
	public boolean TesterEmailIsExisting(String primaryEmail, Connection conn) throws SQLException {
		String sql = "SELECT * FROM Tester WHERE primaryEmail = ?";
		Object[] parameters = {primaryEmail};
		return isExisting(sql, parameters, conn);
	}
	@Override
	public boolean TesterIDIsExisting(int TesterID, Connection conn) throws SQLException {
		String sql = "SELECT * FROM Tester WHERE TesterID = ?";
		Object[] parameters = {TesterID};
		return isExisting(sql, parameters, conn);
	}	
	//not finish
	@Override
	public ArrayList findTesters(Object[] parameters, Connection conn) throws SQLException {
		String sql = "SELECT * FROM Tester WHERE city = CASE WHEN ? = '*' THEN city ELSE ?" + 
		" AND state = CASE WHEN ? = '*' THEN state ELSE ? END" + 
		" AND country = CASE WHEN ? = '*' THEN country ELSE ? END" + 
		" AND company = CASE WHEN ? = '*' THEN company ELSE ? END" + 
		" AND dukeAlumn = CASE WHEN ? = '*' THEN dukeAlumn ELSE ? END" + 
		" AND dukeParent = CASE WHEN ? = '*' THEN dukeParent ELSE ? END" + 
		" AND playGolf = CASE WHEN ? = '*' THEN playGolf ELSE ? END";
		return findObjs(sql, Tester.class, Object[] parameters, conn);
	}	
}