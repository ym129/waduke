package com.WaDuke.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import com.WaDuke.bean.Comment;

public class CommentDAOPostgresqlImpl extends baseDAO implements ICommentDAO {

	@Override
	public int addComment(Comment comment, Connectiion conn) throws SQLException {
		String sql = "INSERT INTO Comment VALUES(DEFAULT," + 
					 "?, ?, ?, ?, ?, ?, ?)";
		Object[] parameters = comment.getAll();
		return basicOperation(sql, parameters);
	}
	@Override
	public int removeComment(int commentID, Connectiion conn) throws SQLException {
		String sql = "DELETE FROM Comment WHERE commentID = ?";
		Object[] params = {commentID};
		return basicOperation(sql, params);
	}
	@Override
	public int updateComment(Comment comment, Connectiion conn) throws SQLException {
		String sql = "UPDATE Comment SET membershipID=?,issues=?,compliments=?,otherComment=?,"+
		"commentDate=?,lapsedMember=?,dataMembershipLapsed=? where CommentID = ?";
		Object[] parameters = comment.getAll();
		return basicOperation(sql, parameters);
	}
	@Override
	public Comment findComment(int commentID, Connectiion conn) throws SQLException {
		String sql = "SELECT * FROM Comment WHERE commentID = " + commentID;
		return (Comment) findObj(sql, Comment.class);
	}
	@Override
	public boolean commentIDIsExisting(int commentID, Connection conn) throws SQLException {
		String sql = "SELECT * FROM Comment WHERE commentID = ?";
		Object[] parameters = {commentID};
		return isExisting(sql, parameters, conn);
	}
}