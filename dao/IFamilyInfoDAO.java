package com.WaDuke.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import com.WaDuke.bean.FamilyInfo;

public interface IFamilyInfoDAO
{
    public int addFamilyInfo(FamilyInfo familyInfo, Connection conn) throws SQLException;
    
    public int removeFamilyInfo(int familyInfoID, Connection conn) throws SQLException;
    
    public int modifyFamilyInfo(FamilyInfo familyInfo, Connection conn) throws SQLException;

    public FamilyInfo findFamilyInfo(int familyInfoID, Connection conn) throws SQLException;

    public boolean familyInfoIDIsExisting(int familyInfoID, Connection conn) throws SQLException;
}