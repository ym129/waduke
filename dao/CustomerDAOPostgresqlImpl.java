package com.WaDuke.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import com.WaDuke.bean.Customer;

public class CustomerDAOPostgresqlImpl extends baseDAO implements ICustomerDAO {

	@Override
	public int addCustomer(Customer customer, Connection conn) throws SQLException {
		String sql = "INSERT INTO Customer VALUES(DEFAULT," + 
					 "?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
					 "?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
					 "?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
					 "?)";
		Object[] parameters = customer.getAll();
		return basicOperation(sql, parameters, conn);
	}

	@Override
	public int removeCustomer(int customerID, Connection conn) throws SQLException {
		String sql = "DELETE FROM Customer WHERE customerID = ?";
		Object[] parameters = {customerID};
		return basicOperation(sql, parameters, conn);
	}
	@Override
	public int updateCustomer(Customer customer, Connection conn) throws SQLException {
		String sql = "UPDATE Customer SET salutation=?, firstName=?, middleName=?, "+
		"nickname=?, lastName=?, suffix=?, address1=?, address2=? "+
		"city=?, state=?, zip=?, country=?, workPhone=?, homePhone=?, cellPhone=?, "+
		"intlPhone=?, primaryEmail=?, alternativeEmail=?, company=?, positionhHeld=?, "+
		"birthday=?, referredBy1=?, referredBy2=?, purposeForVisiting1=?, purposeForVisiting2=?, "+
		"dukeParent=?, gradYear=?, dukeStudentAthlete=?, sport=?, dukeAlumn=?, playGolf=?"+
		" where customerID = ?";
		Object[] parameters = customer.getAll();
		return basicOperation(sql, parameters, conn);
	}

	@Override
	public Customer findCustomerByID(int customerID, Connection conn) throws SQLException {
		String sql = "SELECT * FROM Customer WHERE customerID = ?";
		Object[] parameters = {customerID};
		return (Customer) findObj(sql, parameters, Customer.class, conn);
	}

	@Override
	public Customer findCustomerByEmail(String primaryEmail, Connection conn) throws SQLException {
		String sql = "SELECT * FROM Customer WHERE primaryEmail = ?";
		Object[] parameters = {primaryEmail};
		return (Customer) findObj(sql, parameters, Customer.class, conn);
	}
	@Override
	public boolean customerIDIsExisting(int customerID, Connection conn) throws SQLException {
		String sql = "SELECT * FROM Customer WHERE customerID = ?";
		Object[] parameters = {customerID};
		return isExisting(sql, parameters, conn);
	}
	@Override
	public boolean customerEmailIsExisting(String primaryEmail, Connection conn) throws SQLException {
		String sql = "SELECT * FROM Customer WHERE primaryEmail = ?";
		Object[] parameters = {primaryEmail};
		return isExisting(sql, parameters, conn);
	}	
	//not finish
	@Override
	public ArrayList findCustomers(Object[] parameters, Connection conn) throws SQLException {
		String sql = "SELECT * FROM Customer WHERE city = CASE WHEN ? = '*' THEN city ELSE ?" + 
		" AND state = CASE WHEN ? = '*' THEN state ELSE ? END" + 
		" AND country = CASE WHEN ? = '*' THEN country ELSE ? END" + 
		" AND company = CASE WHEN ? = '*' THEN company ELSE ? END" + 
		" AND dukeAlumn = CASE WHEN ? = '*' THEN dukeAlumn ELSE ? END" + 
		" AND dukeParent = CASE WHEN ? = '*' THEN dukeParent ELSE ? END" + 
		" AND playGolf = CASE WHEN ? = '*' THEN playGolf ELSE ? END";
		return findObjs(sql, Customer.class, Object[] parameters, conn);
	}	
}