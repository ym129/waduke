package com.WaDuke.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import com.WaDuke.bean.Customer;

public interface ICustomerDAO
{
    public int addCustomer(Customer customer, Connection conn) throws SQLException;
    
    public int removeCustomer(int customerID, Connection conn) throws SQLException;
    
    public int modifyCustomer(Customer customer, Connection conn) throws SQLException;

    public Customer findCustomerByID(int customerID, Connection conn) throws SQLException;

    public Customer findCustomerByEmail(String primaryEmail, Connection conn) throws SQLException;

	public boolean customerEmailIsExisting(String primaryEmail, Connection conn) throws SQLException;

	public boolean customerIDIsExisting(int customerID, Connection conn) throws SQLException;
    //not finish
    public ArrayList findCustomers(Object[] parameters, Connection conn) throws SQLException;
}