package com.WaDuke.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import com.WaDuke.bean.Assistant;

public interface IAssistantDAO
{
    public int addAssistant(Assistant assistant, Connection conn) throws SQLException;
    
    public int removeAssistant(int assistantID, Connection conn) throws SQLException;
    
    public int modifyAssistant(Assistant assistant, Connection conn) throws SQLException;

    public Assistant findAssistant(int assistantID, Connection conn) throws SQLException;

    public boolean assistantIDIsExisting(int assistantID, Connection conn) throws SQLException;
}