package com.WaDuke.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import com.WaDuke.bean.Assistant;

public class AssistantDAOPostgresqlImpl extends baseDAO implements IAssistantDAO {

	@Override
	public int addAssistant(Assistant assistant, Connection conn) throws SQLException {
		String sql = "INSERT INTO Assistant VALUES(DEFAULT," + 
					 "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		Object[] parameters = assistant.getAll();
		return basicOperation(sql, parameters);
	}

	@Override
	public int removeAssistant(int assistantID, Connection conn) throws SQLException {
		String sql = "DELETE FROM Assistant WHERE assistantID = ?";
		Object[] params = {assistantID};
		return basicOperation(sql, params);
	}

	@Override
	public int updateAssistant(Assistant assistant, Connection conn) throws SQLException {
		String sql = "UPDATE Assistant SET membershipID=?, salutation=?, firstName=?,"+
		"lastName=?,suffix=?,workPhone=?,cellPhone=?,intlPhone=?,primaryEmail=?,company=?,positionHeld=? WHERE assistantID=?)";
		Object[] parameters = assistant.getAll();
		return basicOperation(sql, parameters);
	}

	@Override
	public Assistant findAssistant(int assistantID, Connection conn) throws SQLException {
		String sql = "SELECT * FROM Assistant WHERE assistantID = " + assistantID;
		return (Assistant) findObj(sql, Assistant.class);
	}
}