package com.WaDuke.dao;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class baseDAO{
    public int basicOperation(String sql, Object[] parameters, Connection conn){
        PreparedStatement ps = null;
        try{
            ps = conn.prepareStatement(sql);
            ParameterMetaData pm = ps.getParameterMetaData();
            if(parameters.length == pm.getParameterCount()){
                for(int i = 0; i < pm.getParameterCount(); ++i){
                    ps.setObject(i+1, parameters[i]);
                }
                int status = ps.executeUpdate();
                ps.close();
                return status;
            }
            else{
                return 0;
            }
        }
        catch(SQLException e){
            e.printStackTrace();
            return 0;
        }
    }
    private Object mappingObj(ResultSet rs,Class clazz) throws SQLException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
        Object obj = clazz.newInstance();
        Method[] methods = clazz.getMethods();
        ResultSetMetaData meta = rs.getMetaData();
        for(int i = 0; i < meta.getColumnCount(); i++){
            String colname = meta.getColumnLabel(i+1);
            String methodname = "set" + colname;
            for(Method method : methods){
                if(method.getName().toLowerCase().equals(methodname.toLowerCase())){
                    method.invoke(obj, rs.getObject(i+1));
                    break;
                }
            }
        }
        return obj;
    }
    //return ONE user, search by costumer ID, for Member Only
    public Object findObj(String sql, Object[] params, Class clazz, Connection conn){
        PreparedStatement ps =null;
        ResultSet rs = null;
        Object obj = null;
        try {
            ps = conn.prepareStatement(sql);
            ParameterMetaData pm = ps.getParameterMetaData();
            for(int i = 0; i < pm.getParameterCount(); i++){
                ps.setObject(i+1, params[i]);
            }
            rs = ps.executeQuery();
            if(rs.next()) {
                obj = mappingObj(rs, clazz);
            }
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return obj;
    }
    public ArrayList findObjs(String sql,Object[] params,Class clazz, Connection conn){
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList objs = new ArrayList();
        try {
            ps = conn.prepareStatement(sql);
            ParameterMetaData pm = ps.getParameterMetaData();
            for(int i = 0; i < params.length; i++){
                ps.setObject(i+1, params[i]);
            }
            rs = ps.executeQuery();
            while(rs.next()){
                Object obj = mappingObj(rs,clazz);
                objs.add(obj);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return objs;
    }
    public boolean isExisting(String sql, Object[] parameters, Connection conn){
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement(sql);
            ParameterMetaData pm = ps.getParameterMetaData();
            for(int i = 0; i < pm.getParameterCount(); i++){
                ps.setObject(i+1, parameters[i]);
            }
            rs = ps.executeQuery();
            boolean ans = rs.next();
            ps.close();
            return ans;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
