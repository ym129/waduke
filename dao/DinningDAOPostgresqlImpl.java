package com.WaDuke.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import com.WaDuke.bean.Dinning;

public class DinningDAOPostgresqlImpl extends baseDAO implements IDinningDAO {

	@Override
	public int addDinning(Dinning dinning, Connection conn) throws SQLException {
		String sql = "INSERT INTO Dinning VALUES(DEFAULT," + 
					 "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		Object[] parameters = dinning.getAll();
		return basicOperation(sql, parameters);
	}

	@Override
	public int removeDinning(int dinningID, Connection conn) throws SQLException {
		String sql = "DELETE FROM Dinning WHERE dinningID = ?";
		Object[] params = {dinningID};
		return basicOperation(sql, params);
	}
	@Override
	public int updateDinning(Dinning dinning, Connection conn) throws SQLException {
		String sql = "UPDATE Dinning SET membershipID=?,favoriteDiningTime=?,tableLocation=?"+
		"wine=?,varietal=?,favoriteServer=?,dietaryNeeds=?,foodAllegies=?,eventsInterestedIn1=?,"+
		"eventsInterestedIn2=?,eventsAttended=?"+
		" where DinningID = ?";
		Object[] parameters = dinning.getAll();
		return basicOperation(sql, parameters);
	}
	@Override
	public Dinning findDinning(int dinningID, Connection conn) throws SQLException {
		String sql = "SELECT * FROM Dinning WHERE DinningID = " + dinningID;
		return (Dinning) findObj(sql, Dinning.class);
	}
	@Override
	public boolean dinningIDIsExisting(int dinningID, Connection conn) throws SQLException {
		String sql = "SELECT * FROM Dinning WHERE dinningID = ?";
		Object[] parameters = {dinningID};
		return isExisting(sql, parameters, conn);
	}
}