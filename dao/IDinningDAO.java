package com.WaDuke.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import com.WaDuke.bean.Dinning;

public interface IDinningDAO
{
    public int addDinning(Dinning dinning, Connection conn) throws SQLException;
    
    public int removeDinning(int dinningID, Connection conn) throws SQLException;
    
    public int modifyDinning(Dinning dinning, Connection conn) throws SQLException;

    public Dinning findDinning(int dinningID, Connection conn) throws SQLException;

    public boolean dinningIDIsExisting(int dinningID, Connection conn) throws SQLException;
}