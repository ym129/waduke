package com.WaDuke.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import com.WaDuke.bean.FamilyInfo;

public class FamilyInfoDAOPostgresqlImpl extends baseDAO implements IfamilyInfoDAO {

	@Override
	public int addFamilyInfo(FamilyInfo familyInfo, Connection conn) throws SQLException {
		String sql = "INSERT INTO FamilyInfo VALUES(DEFAULT," + 
					 "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		Object[] parameters = familyInfo.getAll();
		return basicOperation(sql, parameters, conn);
	}

	@Override
	public int removeFamilyInfo(int familyInfoID, Connection conn) throws SQLException {
		String sql = "DELETE FROM FamilyInfo WHERE familyInfoID = ?";
		Object[] parameters = {familyInfoID};
		return basicOperation(sql, parameters, conn);
	}

	@Override
	public int updateFamilyInfo(FamilyInfo familyInfo, Connection conn) throws SQLException {
		String sql = "UPDATE FamilyInfo SET membershipID=?, martialStatus=?, spouseName=?, "+
		"spouseBirthday=?, anniversary=?, child1Name=?, child1DOB=?, child2Name=? "+
		"child2DOB=?, child3Name=?, child3DOB=?"+
		" where familyInfoID = ?";
		Object[] parameters = familyInfo.getAll();
		return basicOperation(sql, parameters, conn);
	}

	@Override
	public familyInfo findFamilyInfo(int familyInfoID, Connection conn) throws SQLException {
		String sql = "SELECT * FROM FamilyInfo WHERE familyInfoID = ?";
		Object[] parameters = {familyInfoID};
		return (familyInfo) findObj(sql, parameters, FamilyInfo.class, conn);
	}

	@Override
	public boolean familyInfoIDIsExisting(int familyInfoID, Connection conn) throws SQLException {
		String sql = "SELECT * FROM FamilyInfo WHERE familyInfoID = ?";
		Object[] parameters = {familyInfoID};
		return isExisting(sql, parameters, conn);
	}	
}