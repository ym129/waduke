package com.WaDuke.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import com.WaDuke.bean.Transaction;

public interface ITransactionDAO
{
    public int addTransaction(Transaction transaction, Connection conn) throws SQLException;
    
    public int removeTransaction(int transactionID, Connection conn) throws SQLException;
    
    public int modifyTransaction(Transaction transaction, Connection conn) throws SQLException;

    public Transaction findTransaction(int transactionID, Connection conn) throws SQLException;

    public boolean transactionIDIsExisting(int transactionID, Connection conn) throws SQLException;
}