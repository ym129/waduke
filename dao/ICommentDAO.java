package com.WaDuke.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import com.WaDuke.bean.Comment;

public interface ICommentDAO
{
    public int addComment(Comment comment, Connection conn) throws SQLException;
    
    public int removeComment(int commentID, Connection conn) throws SQLException;
    
    public int modifyComment(Comment comment, Connection conn) throws SQLException;

    public Comment findComment(int commentID, Connection conn) throws SQLException;

    public boolean commentIDIsExisting(int commentID, Connection conn) throws SQLException;
}