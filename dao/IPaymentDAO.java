package com.WaDuke.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import com.WaDuke.bean.Payment;

public interface IPaymentDAO
{
    public int addPayment(Payment payment, Connection conn) throws SQLException;
    
    public int removePayment(int paymentID, Connection conn) throws SQLException;
    
    public int modifyPayment(Payment payment, Connection conn) throws SQLException;

    public Payment findPayment(int paymentID, Connection conn) throws SQLException;

    public boolean paymentIDIsExisting(int paymentID, Connection conn) throws SQLException;
}