package com.WaDuke.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import com.WaDuke.bean.Role;

public class RoleDAOPostgresqlImpl extends baseDAO implements IRoleDAO {

	@Override
	public int addRole(Role role) throws SQLException {
		String sql = "INSERT INTO Role VALUES(" + 
					 "?, ?, ?)";
		Object[] parameters = role.getAll();
		return basicOperation(sql, parameters);
	}
	@Override
	public int removeRole(int roleID) throws SQLException {
		String sql = "DELETE FROM Role WHERE roleID = ?";
		Object[] params = {roleID};
		return basicOperation(sql, params);
	}
	@Override
	public int updateRole(Role role) throws SQLException {
		String sql = "UPDATE Role SET customerID=?,membershipID=?,isOwner=?"+
		" where RoleID = ?";
		Object[] parameters = role.getAll();
		return basicOperation(sql, parameters);
	}
	@Override
	public Role findRole(int roleID) throws SQLException {
		String sql = "SELECT * FROM Role WHERE RoleID = " + roleID;
		return (Role) findObj(sql, Role.class);
	}
	@Override
	public boolean roleIDIsExisting(int roleID, Connection conn) throws SQLException {
		String sql = "SELECT * FROM Role WHERE roleID = ?";
		Object[] parameters = {roleID};
		return isExisting(sql, parameters, conn);
	}
	@Override
	public Membership findMembershipByCustomerID(int customerID, Connection conn) throws SQLException{
		String sql = "SELECT * FROM ROLES WHERE customerID=?"
		Object[] parameters = {customerID};
		return findObjs(sql, parameters, Roles.class, conn);
	}
	@Override
    public Customer[] findCustomerByMembershipNum(int number, Connection conn) throws SQLException{
    	String sql = "SELECT * FROM ROLES WHERE membershipNum=?"
		Object[] parameters = {number};
		return findObjs(sql, parameters, Roles.class, conn);
    }

}