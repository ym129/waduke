package com.WaDuke.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import com.WaDuke.bean.Payment;

public class PaymentDAOPostgresqlImpl extends baseDAO implements IPaymentDAO {

	@Override
	public int addPayment(Payment payment, Connection conn) throws SQLException {
		String sql = "INSERT INTO Payment VALUES(DEFAULT," + 
					 "?, ?, ?, ?, ?, ?, ?, ?, ?)";
		Object[] parameters = payment.getAll();
		return basicOperation(sql, parameters, conn);
	}

	@Override
	public int removePayment(int paymentID, Connection conn) throws SQLException {
		String sql = "DELETE FROM Payment WHERE PaymentID = ?";
		Object[] parameters = {paymentID};
		return basicOperation(sql, parameters, conn);
	}

	@Override
	public int updatePayment(Payment payment, Connection conn) throws SQLException {
		String sql = "UPDATE Payment SET membershipID=?, numberOfCard=?, paymentAmount=?, "+
		"paymentTyep=?, checkAcctOnline=?, receivedVia=?, dataPaymentMake=?, secondDateCardSent=? "+
		"secondDateCardSent=?"+
		" where PaymentID = ?";
		Object[] parameters = payment.getAll();
		return basicOperation(sql, parameters, conn);
	}

	@Override
	public Payment findPayment(int paymentID, Connection conn) throws SQLException {
		String sql = "SELECT * FROM Payment WHERE PaymentID = ?";
		Object[] parameters = {paymentID};
		return (Payment) findObj(sql, parameters, Payment.class, conn);
	}

	@Override
	public boolean PaymentIDIsExisting(int paymentID, Connection conn) throws SQLException {
		String sql = "SELECT * FROM Payment WHERE PaymentID = ?";
		Object[] parameters = {paymentID};
		return isExisting(sql, parameters, conn);
	}	
}