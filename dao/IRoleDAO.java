package com.WaDuke.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import com.WaDuke.bean.Role;
import com.WaDuke.bean.Customer;
import com.WaDuke.bean.Membership;

public interface IRoleDAO
{
    public int addRole(Role role, Connection conn) throws SQLException;
    
    public int removeRole(int roleID, Connection conn) throws SQLException;
    
    public int modifyRole(Role role, Connection conn) throws SQLException;

    public Role findRole(int roleID, Connection conn) throws SQLException;

    public boolean roleIDIsExisting(int roleID, Connection conn) throws SQLException;

    public Membership[] findMembershipByCustomerID(String customerID, Connection conn) throws SQLException;

    public Customer[] findCustomerByMembershipNum(String number, Connection conn) throws SQLException;
}