package com.WaDuke.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import com.WaDuke.bean.Manager;

public interface IManagerDAO
{
    public int addManager(Manager manager, Connection conn) throws SQLException;
    
    public int removeManager(int managerID, Connection conn) throws SQLException;
    
    public int modifyManager(Manager manager, Connection conn) throws SQLException;

    public Manager findManager(int managerID, Connection conn) throws SQLException;

    public boolean managerIDIsExisting(int managerID, Connection conn) throws SQLException;
}