package com.WaDuke.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import com.WaDuke.bean.Manager;

public class ManagerDAOPostgresqlImpl extends baseDAO implements IManagerDAO {

	@Override
	public int addManager(Manager manager, Connection conn) throws SQLException {
		String sql = "INSERT INTO Manager VALUES(DEFAULT," + 
					 "?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		Object[] parameters = manager.getAll();
		return basicOperation(sql, parameters, conn);
	}

	@Override
	public int removeManager(int managerID, Connection conn) throws SQLException {
		String sql = "DELETE FROM Manager WHERE ManagerID = ?";
		Object[] parameters = {managerID};
		return basicOperation(sql, parameters, conn);
	}

	@Override
	public int updateManager(Manager manager, Connection conn) throws SQLException {
		String sql = "UPDATE Manager SET salutation=?, firstName=?, lastName=?, "+
		"suffix=?, workPhone=?, cellPhone=?, intlPhone=?, primaryEmail=? "+
		"company=?, positionHeld=?"+
		" where ManagerID = ?";
		Object[] parameters = manager.getAll();
		return basicOperation(sql, parameters, conn);
	}

	@Override
	public Manager findManager(int managerID, Connection conn) throws SQLException {
		String sql = "SELECT * FROM Manager WHERE ManagerID = ?";
		Object[] parameters = {managerID};
		return (Manager) findObj(sql, parameters, Manager.class, conn);
	}

	@Override
	public boolean managerIDIsExisting(int managerID, Connection conn) throws SQLException {
		String sql = "SELECT * FROM Manager WHERE ManagerID = ?";
		Object[] parameters = {managerID};
		return isExisting(sql, parameters, conn);
	}	
}