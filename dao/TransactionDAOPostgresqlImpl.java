package com.WaDuke.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import com.WaDuke.bean.Transaction;

public class TransactionDAOPostgresqlImpl extends baseDAO implements ITransactionDAO {

	@Override
	public int addTransaction(Transaction transaction, Connection conn) throws SQLException {
		String sql = "INSERT INTO Transaction VALUES(DEFAULT," + 
					 "?, ?, ?, ?, ?, ?)";
		Object[] parameters = transaction.getAll();
		return basicOperation(sql, parameters, conn);
	}

	@Override
	public int removeTransaction(int transactionID, Connection conn) throws SQLException {
		String sql = "DELETE FROM Transaction WHERE TransactionID = ?";
		Object[] parameters = {transactionID};
		return basicOperation(sql, parameters, conn);
	}

	@Override
	public int updateTransaction(Transaction transaction, Connection conn) throws SQLException {
		String sql = "UPDATE Transaction SET table=?, primaryKey=?, field=?, "+
		"action=?, originalInfo=?, newInfo=?"+
		" where TransactionID = ?";
		Object[] parameters = transaction.getAll();
		return basicOperation(sql, parameters, conn);
	}

	@Override
	public Transaction findTransaction(int transactionID, Connection conn) throws SQLException {
		String sql = "SELECT * FROM Transaction WHERE TransactionID = ?";
		Object[] parameters = {transactionID};
		return (Transaction) findObj(sql, parameters, Transaction.class, conn);
	}

	@Override
	public boolean TransactionIDIsExisting(int transactionID, Connection conn) throws SQLException {
		String sql = "SELECT * FROM Transaction WHERE TransactionID = ?";
		Object[] parameters = {transactionID};
		return isExisting(sql, parameters, conn);
	}	
}