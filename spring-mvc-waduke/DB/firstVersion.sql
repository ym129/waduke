CREATE TABLE Assistant( --more than 1
	assistantID   int not null,
	salutation    varchar,
	firstName     varchar not null, -- null?
	lastName      varchar not null, -- null?
	suffix        varchar,
	workPhone     char,
	cellPhone     char,
	intlPhone     char,
	primaryEmail  varchar, not null, -- null?
	company       varchar,
	positionHeld  varchar,
   	PRIMARY KEY(assistantID)
);

--one of three comments has to be not null?
CREATE TABLE Comment(
	commentID     int not null,
	issues varchar(500),
	compliments varchar(500),
	otherComment varchar(500),
	commentDate date not null,
	lapsedMember varchar(50),
	dataMembershipLapsed date not null,
   	PRIMARY KEY(commentID)
);

CREATE TABLE Customer(  -- primary member data?
	customerID     int not null,
	salutation varchar,
	firstName varchar not null,
	middleName varchar,
	nickname varchar,
	lastName varchar not null,
	suffix varchar,
	address1 varchar not null,
	address2 varchar,
	city varchar not null,
	state varchar not null,
	zip varchar not null,
	country varchar,
	workPhone     char(10),
	homePhone     char(10),
	cellPhone     char(10),
	intlPhone     char(10),
	primaryEmail varchar not null,
	alternativeEmail varchar,
	company varchar,
	positionhHeld varchar,
	birthday date,
	referredBy1 varchar,--12 options
	referredBy2 varchar,--12 options
	purposeForVisiting1 varchar, --14 options
	purposeForVisiting2 varchar, --14 options
	dukeParent bit,
	gradYear char(4),
	dukeStudentAthlete bit,
	sport varchar,
	dukeAlumn bit,
	playGolf bit,
   	PRIMARY KEY(customerID)
);

CREATE TABLE Dining(
	diningID int not null,
	favoriteDiningTime date, --choice? multi choice?
	tableLocation varchar,   --choice? multi choice?
	wine varchar,            --choice? multi choice?
	varietal varchar,
	favoriteServer varchar,
	dietaryNeeds varchar,
	foodAllegies varchar,
	eventsInterestedIn1 varchar, --14 options
	eventsInterestedIn2 varchar, --14 options
	eventsAttended varchar,      --14 options
	PRIMARY KEY(diningID)
);

CREATE TABLE FamilyInfo(
	familyInfoID int not null,
	martialStatus varchar,   --choice?
	spouseName varchar,
	spouseBirthday varchar,
	spouseBirthday date,
	anniversary date,
	child1Name varchar,
	child1DOB date,
	child2Name varchar,
	child2DOB date,
	child3Name varchar,
	child3DOB date,
	PRIMARY KEY(familyInfoID)
);

CREATE TABLE Manager(
	managerID   int not null,
	salutation    varchar,
	firstName     varchar not null,
	lastName      varchar not null,
	suffix        varchar,
	workPhone     char,
	cellPhone     char,
	intlPhone     char,
	primaryEmail  varchar, not null,
	company       varchar,
	positionHeld  varchar,
   	PRIMARY KEY(managerID)
);

CREATE TABLE Membership(
	membershipNumber int not null,
	accountStatus varchar not null, --active/inactive
	classification varchar not null,--classic/classicPlus/platinum/platinumPlus
	membershipType varchar not null,--enrollment/renewal/upgrade/downgrade/comp
	dataIssued date not null,
	memberSince date,
	yearsMember int,
	expires date not null,
	specificReasonMemberDropped1 varchar,--32 options
	specificReasonMemberDropped2 varchar,--32 options
	overallReasonMemberDropped varchar   --13 options
	PRIMARY KEY(membershipNumber)
);

CREATE TABLE Payment(
	paymentID int not null,
	numberOfCard int,
	paymentAmount int not null,
	paymentTyep varchar, --choice?
	checkAcctOnline varchar,
	receivedVia varchar, --choice?
	dataPaymentMake date not null, -- auto
	dataPacketSent date,
	secondDateCardSent date,
	PRIMARY KEY(paymentID)
);

CREATE TABLE Transaction(
	transactionID int not null,
	table varchar,
	primaryKey varchar,
	field varchar,
	action varchar,
	originalInfo varchar,
	newInfo varchar,
	PRIMARY KEY(transactionID)
);