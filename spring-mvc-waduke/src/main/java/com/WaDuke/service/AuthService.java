package com.WaDuke.service;

import com.WaDuke.bean.Customer;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

public interface AuthService {
    boolean isExistCustomer(String primaryEmail) throws ServletException, IOException, SQLException;//using input email to check if the customer exist 
    boolean validateCustomer(Customer customer) throws SQLException, ServletException, IOException;//check if email correspond with password
}
