package com.WaDuke.service.impl;

import com.WaDuke.bean.Customer;
import com.WaDuke.common.utils.DAOFactory;
import com.WaDuke.dao.ICustomerDAO;
import com.WaDuke.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;


@Service("SearchService")
public class SearchServiceImpl implements SearchService {
    //@Override
   /* public ArrayList findCustomers(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException,SQLException {

    }*/
    @Override
    public Customer findCustomer(HttpServletRequest request, HttpServletResponse responses)throws ServletException, IOException,SQLException  {
       Customer found=new Customer();
        try {
            ICustomerDAO icustomerDAO=(ICustomerDAO)DAOFactory.newInstance("com.WaDuke.dao.ICustomerDAO") ;
            String ID=request.getParameter("customerID");
            found=icustomerDAO.findCustomer(ID);
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
       return found;
    }

}
