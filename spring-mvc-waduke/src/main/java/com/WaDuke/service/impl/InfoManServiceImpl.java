package com.WaDuke.service.impl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.WaDuke.bean.Customer;
import com.WaDuke.dao.ICustomerDAO;
import com.WaDuke.common.utils.DAOFactory;
import com.WaDuke.service.InfoManService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;


@Service("InfoManService")
public class InfoManServiceImpl extends HttpServlet implements InfoManService {



    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub

        Customer customer = new Customer();
        //customer.setBirthday(request.getParameter("birthday"));
        customer.setCity(request.getParameter("city"));
        customer.setCountry(request.getParameter("country"));
        customer.setCompany(request.getParameter("company"));
        customer.setFirstName(request.getParameter("first name"));
        customer.setSalutation(request.getParameter("salutation"));
        customer.setMiddleName(request.getParameter("middle name"));
        customer.setNickName(request.getParameter("nick name"));
        customer.setLastName(request.getParameter("last name"));
        customer.setSuffix(request.getParameter("suffix"));
        customer.setState(request.getParameter("state"));
        customer.setZip(request.getParameter("zip"));
        customer.setWorkPhone(request.getParameter("work phone"));
        customer.setHomePhone(request.getParameter("home phone"));
        customer.setCellPhone(request.getParameter("cellphone"));
        customer.setIntlPhone(request.getParameter("intl phone"));
        customer.setPrimaryEmail(request.getParameter("primary email"));
        customer.setAlternativeEmail(request.getParameter("alternative email"));
        customer.setPositionHeld(request.getParameter("position"));
        customer.setReferredBy1(request.getParameter("refer1"));
        customer.setReferredBy2(request.getParameter("refer2"));
        customer.setPurposeForVisiting1(request.getParameter("purpose for visiting1"));
        customer.setPurposeForVisiting2(request.getParameter("purpose for visiting2"));
        customer.setGradYear(request.getParameter("grade year"));
        customer.setSport(request.getParameter("set sport"));
        try {
            ICustomerDAO icustomerDAO=(ICustomerDAO)DAOFactory.newInstance("com.WaDuke.dao.ICustomerDAO") ;
            icustomerDAO.addCustomer(customer);
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        response.sendRedirect("action?actiontype=detail&customerid="+String.valueOf(customer.getCustomerID()));
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        doGet(request, response);
    }


    @Override
    public void insert(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

  /*  @Override
    public void update(Customer cust) {
        icustomerDAO.update(cust);
    }

    @Override
    public void delete(long id) {
        icustomerDAO.delete(id);
    }*/

}
