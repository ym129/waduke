package com.WaDuke.service.impl;

import com.WaDuke.bean.Customer;
import com.WaDuke.common.utils.DAOFactory;
import com.WaDuke.dao.ICustomerDAO;
import com.WaDuke.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;


@Service("AuthService")
public class AuthServiceImpl implements AuthService {
    ICustomerDAO iCustomerDAO = null;
    public boolean isExistCustomer(String primaryEmail) throws ServletException, IOException, SQLException{
	iCustomerDAO = (ICustomerDAO) DAOFactory.newInstance("ICustomerDAO");
	Customer customer = iCustomerDAO.findCustomer(primaryEmail);
	if(customer != null)
	    return true;
	else return false;
    }
    public boolean validateCustomer(Customer customer) throws ServletException, IOException, SQLException {
	ICustomerDAO iCustomerDAO = (ICustomerDAO) DAOFactory.newInstance("ICustomerDAO");
	Customer customer = iCustomerDAO.findCustomer(customer.getprimaryEmail());
	if(customer.getPassword().equals(customer.getPassword()))
	    return true;
	else return false;
    }

}
