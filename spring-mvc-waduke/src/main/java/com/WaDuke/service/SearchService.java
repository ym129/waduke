package com.WaDuke.service;

import com.WaDuke.bean.Customer;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

public interface SearchService {


    //ArrayList findCustomers(HttpServletRequest request, HttpServletResponse responseD) throws ServletException, IOException,SQLException;

    Customer findCustomer(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,SQLException;

}
