package com.WaDuke.bean;

import java.sql.Timestamp;

public class Comment {
    private int commentID;
    private String issues;
    private String compliments;
    private String otherComments;
    private Timestamp commentDate;
    private String lapsedMember;
    private Timestamp dateMembershipLapsed;

    public int getCommentID(){
        return commentID;
    }
    public void setCommentID(int ID){
        commentID = ID;
    }

    public String getIssues(){
        return issues;
    }
    public void setIssues(String issues){
        this.issues = issues;
    }

    public String getCompliments(){
        return compliments;
    }
    public void setCompliments(String compliments){
        this.compliments = compliments;
    }

    public String getOtherCompliments(){
        return otherComments;
    }
    public void setOtherCompliments(String otherComments){
        this.otherComments = otherComments;
    }

    public Timestamp setCommentDate(){
        return commentDate;
    }
    public void getCommentDate(Timestamp commentDate){
        this.commentDate = commentDate;
    }

    public String getLapsedMember(){
        return lapsedMember;
    }
    public void setLapsedMember(String lapsedMember){
        this.lapsedMember = lapsedMember;
    }

    public Timestamp setDateMemberLapsed(){
        return dateMembershipLapsed;
    }
    public void getDateMemberLapsed(Timestamp dateMembershipLapsed){
        this.dateMembershipLapsed = dateMembershipLapsed;
    }
}
