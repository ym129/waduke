package com.WaDuke.bean;
import java.sql.Timestamp;
public class FamilyInfo {
    private int familyInfoID;
    private String martialStatus;
    private String spouseName;
    private Timestamp spouseBirthday;
    private Timestamp anniversary;
    private String child1Name;
    private Timestamp child1DOB;
    private String child2Name;
    private Timestamp child2DOB;
    private String child3Name;
    private Timestamp child3DOB;

    public int getFamilyInfoID(){
        return familyInfoID;
    }
    public void setFamilyInfoID(int ID){
        familyInfoID = ID;
    }
    public String getMartialStatus(){
        return martialStatus;
    }
    public void setMartialStatus(String martialStatus){
        this.martialStatus = martialStatus;
    }
    public String getSpouseName(){
        return spouseName;
    }
    public void setSpouseName(String spouseName){
        this.spouseName = spouseName;
    }
    public Timestamp getSpouseBirthday(){
        return spouseBirthday;
    }
    public void setSpouseBirthday(Timestamp spouseBirthday){
        this.spouseBirthday = spouseBirthday;
    }
    public Timestamp getAnniversary(){
        return anniversary;
    }
    public void setAnniversary(Timestamp anniversary){
        this.anniversary = anniversary;
    }
    public String getChild1Name(){
        return child1Name;
    }
    public void setChild1Name(String child1Name){
        this.child1Name = child1Name;
    }
    public Timestamp getChild1DOB(){
        return child1DOB;
    }
    public void setChild1DOB(Timestamp child1DOB){
        this.child1DOB = child1DOB;
    }
    public String getChild2Name(){
        return child2Name;
    }
    public void setChild2Name(String child2Name){
        this.child2Name = child2Name;
    }
    public Timestamp getChild2DOB(){
        return child2DOB;
    }
    public void setChild2DOB(Timestamp child2DOB){
        this.child2DOB = child2DOB;
    }
    public String getChild3Name(){
        return child3Name;
    }
    public void setChild3Name(String child3Name){
        this.child3Name = child3Name;
    }
    public Timestamp getChild3DOB(){
        return child3DOB;
    }
    public void setChild3DOB(Timestamp child3DOB){
        this.child3DOB = child3DOB;
    }
}