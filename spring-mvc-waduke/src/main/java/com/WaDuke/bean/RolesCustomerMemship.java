package com.WaDuke.bean;

import java.sql.Timestamp;

public class RolesCustomerMemship {
    private int customerID;
    private int membershipID;
    private boolean isOwner;
    public int getCustomerID(){
        return customerID;
    }
    public void setCustomerID(int ID){
        customerID = ID;
    }
    public int getmembershipID(){
        return membershipID;
    }
    public void setmembershipID(int ID){
        membershipID = ID;
    }
    public boolean getIsOwner(){
        return isOwner;
    }
    public void setIsOwner(boolean isOwner){
        this.isOwner = isOwner;
    }
}
