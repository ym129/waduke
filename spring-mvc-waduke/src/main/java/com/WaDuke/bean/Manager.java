package com.WaDuke.bean;

import java.sql.Timestamp;

//adding permission field?

public class Manager {
    private int managerID;
    private String salutation;
    private String firstName;
    private String lastName;
    private String suffix;
    private String workPhone;
    private String cellPhone;
    private String intlPhone;
    private String primaryEmail;
    private String company;
    private String positionHeld;

    public int getManagerID(){
        return managerID;
    }
    public void setManagerID(int ID){
        managerID = ID;
    }

    public String getSalution(){
        return salutation;
    }
    public void setSalution(String salutation){
        this.salutation = salutation;
    }

    public String getFirstName(){
        return firstName;
    }
    public void setFirstName(String firstName){
        this.firstName = firstName;
    }

    public String getLastName(){
        return lastName;
    }
    public void setLastName(String lastName){
        this.lastName = lastName;
    }

    public String getSuffix(){
        return suffix;
    }
    public void setSuffix(String suffix){
        this.suffix = suffix;
    }

    public String getWorkPhone(){
        return workPhone;
    }
    public void setWorkPhone(String workPhone){
        this.workPhone = workPhone;
    }

    public String getCellPhone(){
        return cellPhone;
    }
    public void setCellPhone(String cellPhone){
        this.cellPhone = cellPhone;
    }

    public String getIntlPhone(){
        return intlPhone;
    }
    public void setIntlPhone(String intlPhone){
        this.intlPhone = intlPhone;
    }

    public String getPrimaryEmail(){
        return primaryEmail;
    }
    public void setPrimaryEmail(String primaryEmail){
        this.primaryEmail = primaryEmail;
    }

    public String getCompany(){
        return company;
    }
    public void setCompany(String company){
        this.company = company;
    }

    public String getPositionHeld(){
        return positionHeld;
    }
    public void setPositionHeld(String positionHeld){
        this.positionHeld = positionHeld;
    }
}
