package com.WaDuke.dao.impl;

import com.WaDuke.bean.Customer;
import com.WaDuke.dao.ICustomerDAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.WaDuke.bean.Customer;

import java.sql.SQLException;
import java.util.ArrayList;



public class CustomerDAOPoImpl extends baseDAO implements ICustomerDAO {

   /* @Override
    public int addCustomer(Customer customer) throws SQLException {
        String sql = "INSERT INTO Customer VALUES(" +
                "?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"+
        "?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"+
    "?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"+
        "?, ?);";
        Object[] parameters = customer.getAll();
        return basicOperation(sql, parameters);
    }

    @Override
    public int removeCustomer(String customerID) throws SQLException {
        String sql = "DELETE FROM Customer WHERE primaryEmail = ?;";
        Object[] params = {customerID};
        return modifyObj(sql, params);
    }
    //UPDATE SET????
    @Override
    public int updateCustomer(Customer customer) throws SQLException {
        String sql = "UPDATE Customer SET ... where customerID = ?;";
        Object[] parameters = {customer.getPassword(), customer.getEmail(), customer.getUsername()} ;
        return modifyObj(sql, parameters);
    }*/

    //return ONE customer, for member view information ONLY
    @Override
    public Customer findCustomer(String ID) throws SQLException {
        String sql = "SELECT * FROM Customer WHERE customerID = " + ID + ";";
        return (Customer) findObj(sql, Customer.class);
    }

    //return ALL customer, for manager report ONLY
    //
    @Override
    public ArrayList findCustomers(Object[] parameters) throws SQLException {
        String sql = "SELECT * FROM Customer WHERE city = ? AND state = ? AND country = ? AND company = ? AND dukeAlumn = ? AND dukeParent = ? AND playGolf = ?;";
        return findObjs(sql, Customer.class, Object[] parameters);
    }

}