package com.WaDuke.dao;

import com.WaDuke.bean.Customer;

import java.util.List;


import java.sql.SQLException;
import java.util.ArrayList;

import com.WaDuke.bean.Customer;

public interface ICustomerDAO
{
    int addCustomer(Customer customer) throws SQLException;

    // int removeCustomer(String string) throws SQLException;

    // public int modifyCustomer(Customer customer) throws SQLException;
    ArrayList findCustomers(Object[] parameter) throws SQLException;
    Customer findCustomer(String ID) throws SQLException;


}
